﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeoWebApi.DTOModel
{
    public class Points
    {
        public string type;
        public Property properties;
        public Geometry geometry;
    }
}