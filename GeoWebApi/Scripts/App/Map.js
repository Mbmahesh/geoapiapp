$(function () {

    var featuresLayer = null;
    var dataMap = null;
    var category = null;
    markers = null;
    function CreateMap() {
        var style = {
            "clickable": true,
            "color": "#00d",
            "fillcolor": "#00d",
            "weight": 1.0,
            "opacity": 0.3,
            "fillopacity": 0.2
        };
        var hoverstyle = {
            "fillopacity": 0.5
        };



        document.getElementById('weathermap').innerHTML = "<div id='map'></div>";
        _fixMapHeight();
        var map = L.map('map').setView([35.679609609368576, -94.932861328125], 4);
        //var osmurl = 'http://{s}.mqcdn.com/tiles/1.0.0/osm/{z}/{x}/{y}.png',
        //    subdomains = ['otile1', 'otile2', 'otile3', 'otile4'],
        //     osmattribution = 'map data © <a href="http://openstreetmap.org">openstreetmap</a> contributors,' +
        //         ' <a href="http://creativecommons.org/licenses/by-sa/2.0/">cc-by-sa</a>',
        //    osmlayer = new L.tileLayer(osmurl, { minZoom: 4, maxzoom: 18, attribution: osmattribution, subdomains: subdomains }).addTo(map);
        var bing = new L.BingLayer("ArPW_PrjdWA0inUUvMd7EbOzfktN7uhDaxtCI5wYdsM3KzjrC1GV1_bAE_apetle");
        map.addLayer(bing);
        var drawnItems = new L.FeatureGroup();
        var drawControl = new L.Control.Draw({
            circle: false,
            polyline: false,
            rectangle: false,
            marker:false,
            edit: {
                featureGroup: drawnItems
            }
        });

        map.addControl(drawControl);
        new L.Control.GeoSearch({
            provider: new L.GeoSearch.Provider.Bing({
            	// request your free key at: bingmapsportal.com
            	key: 'ArPW_PrjdWA0inUUvMd7EbOzfktN7uhDaxtCI5wYdsM3KzjrC1GV1_bAE_apetle'
            }),
            position: 'topcenter',
            showMarker: false
        }).addTo(map);
        markers = new L.MarkerClusterGroup();
        map.on('draw:created', function (e) {
            drawnItems.clearLayers();
            drawnItems.addLayer(e.layer);
            GetPins(map, e.layer.getBounds(), 1, category);
            //map.fitBounds(e.layer.getBounds()); // max zoom to see whole polygon
            //map.setMaxBounds(e.layer.getBounds()); // restrict map view to polygon bounds
            //map.options.minZoom = map.getZoom();
            var z = e.layer.getBounds();
        });

        return map;
    }

    function GetPins(map, bounds, flag, selected) {
        var z = null;
        if (flag === 1) {
            z = bounds;
        }
        else {
            var z = map.getBounds();
        }
        var data = null;
        if (selected === null) {
            data = { South: JSON.stringify(z.getSouth()), North: JSON.stringify(z.getNorth()), West: JSON.stringify(z.getWest()), East: JSON.stringify(z.getEast()) };
        }
        else {
            data = { South: JSON.stringify(z.getSouth()), North: JSON.stringify(z.getNorth()), West: JSON.stringify(z.getWest()), East: JSON.stringify(z.getEast()), category: category };
            debugger;
        }
        $.ajax({
            url: '/home/Get',
            type: "GET",
            data: data,

            success: function (data, status, xhr) {
                dataMap = data.dt.data1;
                InitializeSearchAndMarkers(dataMap, map);
                //  map.fitBounds(map.getBounds());
                $("#spnExp").text(data.dt.expMoney);
                $("#pExpValueRange1").text(data.dt1.expMoney1);
                $("#pExpValueRange2").text(data.dt2.expMoney2);
                $("#pExpValueRange3").text(data.dt3.expMoney3);
                $("#perExpRange1").text(data.dt1.per1);
                $("#perExpRange2").text(data.dt2.per2);
                $("#perExpRange3").text(data.dt3.per3);
                $("#noRange1").text(data.dt1.noRange1);
                $("#noRange2").text(data.dt2.noRange2);
                $("#noRange3").text(data.dt3.noRange3);
            },
            error: function (xhr, status, error) {
                debugger;
                alert(error);
            }

        });
    }

    function InitializeSearchAndMarkers(data, map) {
        var baseballicon1 = L.icon({
            iconUrl: 'Content/leaflet-0.7.3/images/marker-icon-red.png',
            iconSize: [32, 37],
            iconAnchor: [16, 37],
            popupAnchor: [0, -28]
        });
        var baseballicon = L.icon({
            iconUrl: 'Content/leaflet-0.7.3/images/marker-icon-red.png',
            iconSize: [32, 37],
            iconAnchor: [16, 37],
            popupAnchor: [0, -28]
        });
        if (featuresLayer !== null) {
            featuresLayer.clearLayers();
            markers.clearLayers();
        }
        featuresLayer = L.geoJson(data, {
            style: function (feature) {
                switch (feature.properties.Name) {
                    case 'Test1': return { color: "#ff0000" };
                    case 'Test': return { color: "#0000ff" };
                }

            },
            pointToLayer: function (feature, latlng) {
                if (feature.properties.Category === "Earth") {
                    return L.marker(latlng, { icon: baseballicon });
                }
                else {

                    return L.marker(latlng, { icon: baseballicon1 });
                }
            },
            onEachFeature: function (feature, layer) {
                layer.bindPopup('Exposure Amount: '+feature.properties.Loan+'<br />'+'Customer Name: ABC PVT Solutions');
            }
        });
        // map.removeLayer(featuresLayer.re);
        var bounds = featuresLayer.getBounds();
        // map.setView(featuresLayer.get
        map.removeLayer(featuresLayer);
        markers.removeLayer(featuresLayer);
        markers.addLayer(featuresLayer);
        map.addLayer(markers);



        map.addLayer(markers);


        //  map.setMaxBounds([20.548165423046584,- 119.44335937499999]); // restrict map view to polygon bounds
        // map._layersMinZoom = 4;
        // map.fitBounds(featuresLayer.getBounds());
        // map.setView(map.getCenter(), map.getZoom());
        map.setZoom(map.getZoom());


    }

    /* 
     * Make sure that the sidebar is streched full height
     * ---------------------------------------------
     * We are gonna assign a min-height value every time the
     * wrapper gets resized and upon page load. We will use
     * Ben Alman's method for detecting the resize event.
     * 
     **/
    function _fixMapHeight() {
        //Get window height and the wrapper height
        if ($("#map")[0]) {
            var viewportHeight = $(window).height();
            var mapOffsetHeight = $("#map").offset().top;

            $("#map").css("height", (viewportHeight - mapOffsetHeight - 20) + "px");
        }
        if ($("#charts")[0]) {
            var viewportHeight1 = $(window).height();
            var mapOffsetHeight1 = $("#charts").offset().top;

            $("#charts").css("height", (viewportHeight1 - mapOffsetHeight1 - 20) + "px");
        }

    }
    //function DrawChart() {

        
    //}

    function init() {

        //$('input[name="rd"]').on('ifChecked', function (event) {
        //    category = this.value;
        //    GetPins(map, null, 2, category);
        //});
        
        $('#btnFloods').on('click', function (event) {
            category = $('#btnFloods').html();
            GetPins(map, null, 2, category);
        });
        $('#btnEarth').on('click', function (event) {
            category = 'Earth';
            GetPins(map, null, 2, category);
        });
        $('#btnFire').on('click', function (event) {
            category = $('#btnFire').html();
            GetPins(map, null, 2, category);
        });
        $('#btnAll').on('click', function (event) {
            // category = $('#btnAll').html();
            GetPins(map, null, 2, null);
        });
        $(".wrapper").resize(function () {
            _fixMapHeight();
        });

        map = CreateMap();
        _fixMapHeight();
        var z = map.getBounds();
        GetPins(map, null, 2, category);
        map.on('zoomend', function () {
            GetPins(map, null, 2, category);
        });
        map.on('dragend', function () {
            GetPins(map, null, 2, category);
        });
        
        var s1 = [200, 600, 700, 1000];
        var s2 = [460, 210, 690, 820];
        var ticks = ['2009', '2010', '2011', '2012'];
        var plot1 = $.jqplot('myChart', [s1, s2], {
            // The "seriesDefaults" option is an options object that will
            // be applied to all series in the chart.
            seriesDefaults: {
                renderer: $.jqplot.BarRenderer,
                rendererOptions: { fillToZero: true }
            },
            // Custom labels for the series are specified with the "label"
            // option on the series option.  Here a series option object
            // is specified for each series.
            series: [
                { label: 'Invested Amount' },
                { label: 'Amount at Risk' }

            ],
            title: 'Amount at Risk Based on Years',
            // Show the legend and put it outside the grid, but inside the
            // plot container, shrinking the grid to accomodate the legend.
            // A value of "outside" would not shrink the grid and allow
            // the legend to overflow the container.
            legend: {
                show: true,
                placement: 'outsideGrid',
                width: '200px',
            },
            axes: {
                // Use a category axis on the x axis and use our custom ticks.
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                },
                // Pad the y axis just a little so bars can get close to, but
                // not touch, the grid boundaries.  1.2 is the default padding.
                yaxis: {
                    pad: 1.05,
                    tickOptions: { formatString: '$%d' }
                },
                highlighter: {
                    show: true,
                    sizeAdjust: 7.5
                }
            }
        });
        var plot2 = jQuery.jqplot('myChart1', [[['Earth Quake', 9], ['Fire', 5], ['Floods', 4]]],

    {
        title: '% Amount Based on The Disasters ',
        seriesDefaults: {
            shadow: false,
            renderer: jQuery.jqplot.PieRenderer,
            rendererOptions: {
                startAngle: 180,
                sliceMargin: 4,
                showDataLabels: true
            }
        },
        legend: { show: true, location: 'w' }
    });
        var line1 = [['2006-06-30 8:00AM', 100], ['2007-06-30 8:00AM', 400], ['2008-06-30 8:00AM', 500], ['2009-06-30 8:00AM', 100], ['2010-7-30 8:00AM', 200], ['2011-8-30 8:00AM', 100], ['2012-9-30 8:00AM', 50], ['2013-10-30 8:00AM', 825]];
        var plot3 = $.jqplot('myCharts2', [line1], {
            title: 'Previous 8 years Amount at Risk(Trend)',
            gridPadding: { right: 35 },
            axesDefaults: {
                pad: 0
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.DateAxisRenderer,
                    tickOptions: { formatString: '%b %#d, %y' },
                    min: 'May 30, 2008',
                    tickInterval: '12 month'
                }
            },
            yaxis: {
                pad: 1.05,
                tickOptions: { formatString: '$%d' }
            },
            series: [{ lineWidth: 3, markerOptions: { style: 'square' } }]
        });

        $("#tabs").tabs({
            activate: function (event, ui) {
                if (ui.index === 1 && plot1._drawCount === 0 && plot2._drawCount === 0 && plot3._drawCount === 0) {
                    plot1.replot();
                    plot2.replot();
                    plot3.replot();
                }
            }
        });
        //  var dat= DrawChart();
    }

    init();


});


