﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeoWebApi.Controllers
{
    public class Bound
    {
        public End latNE;
        public End lonNE;
      
    }

    public class End
    {
        public double lat;
        public double lon;
    }
}
