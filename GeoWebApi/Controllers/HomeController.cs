﻿using GeoJSON.Net.Geometry;
using GeoWebApi.Models;
using GeoWebApi.DTOModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;


namespace GeoWebApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Get(string South, string North, String West, string East, string category)
        {

            double North1 = Convert.ToDouble(North);
            double West1 = Convert.ToDouble(West);
            double South1 = Convert.ToDouble(South);
            double East1 = Convert.ToDouble(East);
            using (GeoEntities entities = new GeoEntities())
            {
                decimal exp = 0;
                decimal exp1 = 0;
                decimal exp2 = 0;
                decimal exp3 = 0;
                var data = entities.GetPinsWithinBound(South1, North1, West1, East1, category).ToList();
                var data1 = data.Select(o => new Points
                {
                    type = "Feature",
                    properties = new Property
                    {
                        Name = "Test",
                        Loan = String.Format("{0:C}", o.Exposure),
                        Category = o.Category
                    },
                    geometry = new Geometry { type = "Point", coordinates = new double?[] { o.Lon, o.Lat } }
                });
                var range1 = data.Where(a => a.Exposure <= 1000 && a.Exposure >= 0).Select(a => a.Exposure);
                var range2 = data.Where(a => a.Exposure <= 2000 && a.Exposure >= 1000).Select(a => a.Exposure);
                var range3 = data.Where(a => a.Exposure >= 2000).Select(a => a.Exposure);

                string per2 = string.Empty;
                string per3 = string.Empty;
                string per1 = string.Empty;
                int noRange1 = range1.Count();
                int noRange2 = range2.Count();
                int noRange3 = range3.Count();


                foreach (var item in range1)
                {
                    exp1 += Convert.ToDecimal(item.ToString());
                }
                foreach (var item in range2)
                {
                    exp2 += Convert.ToDecimal(item.ToString());
                }
                foreach (var item in range3)
                {
                    exp3 += Convert.ToDecimal(item.ToString());
                }
                foreach (var item in data)
                {
                    exp += Convert.ToDecimal(item.Exposure.ToString());
                }
                if (exp != 0)
                {
                    per1 = (exp1 / exp * 100).ToString("#.##") + "%";
                }
                else
                {
                    per1 = "0" + "%";
                }
                if (exp != 0)
                {
                    per2 = (exp2 / exp * 100).ToString("#.##") + "%";
                }
                else
                {
                    per2 = "0" + "%";
                }
                if (exp != 0)
                {
                    per3 = (exp3 / exp * 100).ToString("#.##") + "%";
                }
                else
                {
                    per3 = "0" + "%";
                }
                //exp=Convert
                string expMoney1 = string.Format("{0:C}", exp1);
                string expMoney = string.Format("{0:C}", exp);
                string expMoney2 = string.Format("{0:C}", exp2);
                string expMoney3 = string.Format("{0:C}", exp3);
                var dt = new { data1, expMoney };
                var dt1 = new { range1, expMoney1, per1, noRange1 };
                var dt2 = new { range2, expMoney2, per2, noRange2 };
                var dt3 = new { range3, expMoney3, per3, noRange3 };

                var fdata = new { dt, dt1, dt2, dt3 };
                return Json(fdata, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Index()
        {



            //  System.Runtime.Serialization.Json.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            return View();

        }

    }
}
