//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GeoWebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Map
    {
        public int id { get; set; }
        public Nullable<double> Lat { get; set; }
        public Nullable<double> Lon { get; set; }
        public Nullable<decimal> Exposure { get; set; }
        public System.Data.Spatial.DbGeography Point { get; set; }
        public string Category { get; set; }
    }
}
