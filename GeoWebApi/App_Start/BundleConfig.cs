﻿using System.Web;
using System.Web.Optimization;

namespace GeoWebApi
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                //"~/Scripts/excanvas.js",
                        "~/Scripts/jquery.js",
                        "~/Scripts/jquery-ui.js",
                      //  "~/Scripts/Chart.js",
                          "~/Scripts/bootstrap.js",
                          "~/Scripts/jquery.jqplot.js",
                           "~/Scripts/plugins/jqplot.barRenderer.js",
                            "~/Scripts/plugins/jqplot.categoryAxisRenderer.js",
                             "~/Scripts/plugins/jqplot.pieRenderer.js",
                             "~/Scripts/plugins/jqplot.dateAxisRenderer.js",
                              "~/Scripts/plugins/jqplot.pointLabels.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                 "~/Content/jquery.jqplot.css",
                "~/Content/site.css",

                "~/Content/ionicons.css",
                "~/Content/font-awesome.css",
                "~/Content/bootstrap.css",
                "~/Content/AdminLTE.css",
                "~/Content/themes/base/jquery-ui.css",
               
               
                "~/Content/bootstrap-theme.css"));

            bundles.Add(new StyleBundle("~/Content/leaflet-0.7.3/css").Include(
                "~/Content/leaflet-0.7.3/leaflet.css",
                "~/Content/leaflet-0.7.3/l.geosearch.css",
                "~/Content/leaflet-0.7.3/MarkerCluster.css",
                "~/Content/leaflet-0.7.3/MarkerCluster.Default.css",
                "~/Content/leaflet-0.7.3/leaflet-search.src.css",
                "~/Content/leaflet-0.7.3/leaflet.draw.css"));

            bundles.Add(new ScriptBundle("~/bundle/leaflet").Include(
                "~/Scripts/GeoWebApi/leaflet.js",
                "~/Scripts/GeoWebApi/leaflet.draw.js",
                "~/Scripts/GeoWebApi/TileLayer.GeoJSON.js",
                "~/Scripts/GeoWebApi/leaflet-search.src.js",
                "~/Scripts/GeoWebApi/leaflet.markercluster-src.js",
                "~/Scripts/GeoWebApi/leaflet.markercluster.js",
                "~/Scripts/GeoWebApi/l.control.geosearch.js",
                "~/Scripts/GeoWebApi/l.geosearch.provider.openstreetmap.js",
                "~/Scripts/GeoWebApi/TileLayer.Bing.js"));

            bundles.Add(new ScriptBundle("~/bundle/app").Include(
               "~/Scripts/App/app.js",
               "~/Scripts/App/map.js"));

        }
    }
}